**"fun":**

| Name | Link                       | Description |
|------|----------------------------|-------------|
| Intranet IRISA | [https://intranet.irisa.fr/](https://intranet.irisa.fr/) | plop |
| Intranet INRIA | [https://intranet.inria.fr/](https://intranet.inria.fr/) | plop |
| Amethis | [amethis.doctorat-bretagneloire.fr/](amethis.doctorat-bretagneloire.fr/) | plop |
| MathStic "Documents de référence" | [https://ed-mathstic.doctorat-bretagneloire.fr/en/node/247](https://ed-mathstic.doctorat-bretagneloire.fr/en/node/247) | plop |
| OSE | [https://ose.univ-rennes1.fr/](https://ose.univ-rennes1.fr/) | plop |

**practical:**

| Name | Link                       | Description |
|------|----------------------------|-------------|
| Gitlab INRIA | [https://gitlab.inria.fr/](https://gitlab.inria.fr/) | plop |
| Big Blue Button | [https://bbb.inria.fr/](https://bbb.inria.fr/) | plop |
| ENT Rennes1 University | [https://ent.univ-rennes1.fr/f/welcome/normal/render.uP](https://ent.univ-rennes1.fr/f/welcome/normal/render.uP) | University mail / calendar |

**fun:**

| Name | Link                       | Description |
|------|----------------------------|-------------|
| Mattermost | [https://mattermost.inria.fr](https://mattermost.inria.fr) | plop |
| Irisa's Discord channel | [https://discord.gg/8sRp8JwPCp](https://discord.gg/8sRp8JwPCp) | plop | 


**newcomer:**

| Name | Link                       | Description |
|------|----------------------------|-------------|
| Newcomer information page | [https://intranet.irisa.fr/en/page/231](https://intranet.irisa.fr/en/page/231) | plop |
| Printer setup | [https://doc-si.inria.fr/display/SU/Impression+unifiee](https://doc-si.inria.fr/display/SU/Impression+unifiee) | plop |
